# Course2021-2022

Resources and assignments for Data and Knowledge Graph Course at KULeuven (2021-2022)

## Add your files

This is the normal process that you will have to follow in order to interact with the repository:

* Fork the main repository into your own account (this will generate a new repository in your GitHub account). This is done only once during the course. 
* If you had already forked the repository some time ago, you may want to sync your repository to the latest version that is now available. This is done by [configuring the remote for a fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) and [syncing your fork](https://about.gitlab.com/blog/2016/12/01/how-to-keep-your-fork-up-to-date-with-its-origin/). Basically, you have to:


* Make any changes to your repository, according to the specific assignment
* Commit your changes into your local repository
* Push your changes to your online repository
* Make a [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/getting_started.html), so that we can check your changes and accept them into the master of the general repository. If everything is ok, your changes will be incorporated into the main repository. If not, you will receive a message of why not.



